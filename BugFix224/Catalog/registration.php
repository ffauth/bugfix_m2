<?php
/**
 * Theme by Imagination Media
 *
 * @category  ImaginationMedia
 * @author    Fernando Fauth <fernando@imaginemage.com>
 * @copyright Copyright (c) 2018 Imagination Media (http://imaginemage.com/)
 * @license   https://opensource.org/licenses/OSL-3.0.php Open Software License 3.0
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'BugFix224_Catalog',
    __DIR__
);
